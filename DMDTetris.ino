/* DMDTetris
   My attempt to play tetris with an Arduino and
   LED DMD.

   Hardware:
   * LED DMD.

   Libraries:
   * DMD2 from Freetronics ( https://github.com/freetronics/DMD2 )

   License:
   GPL3

   Peter Hardy <peter@hardy.dropbear.id.au>
*/
#include <SPI.h>
#include <DMD2.h>

SPIDMD dmd(1, 1);
DMDFrame dmdBuffer(dmd);

// Each int in this array represents a row of the playfield.
// Rows in this array are translated to columns of the DMD.
// Note that the first four rows and the last row are not
// displayed.
const int playFieldRows = 25;
uint16_t playField[playFieldRows];

// First index is tetromino ID.
// Second index is orientation. These are the same order as
// described on https://tetris.wiki/SRS . First index is spawn state.
// Third index is the row.
const uint8_t tetros[7][4][4] = {
  {{ 0,15, 0, 0}, { 2, 2, 2, 2}, { 0, 0,15, 0}, { 2, 2, 2, 2}}, // I
  {{ 8,14, 0, 0}, { 6, 4, 4, 0}, { 0,14, 2, 0}, { 4, 4,12, 0}}, // J
  {{ 2,14, 0, 0}, { 4, 4, 6, 0}, { 0,14, 8, 0}, {12, 4, 4, 0}}, // L
  {{ 6, 6, 0, 0}, { 6, 6, 0, 0}, { 6, 6, 0, 0}, { 6, 6, 0, 0}}, // O
  {{ 6,12, 0, 0}, { 4, 6, 2, 0}, { 0, 6,12, 0}, { 8,12, 4, 0}}, // S
  {{ 4,14, 0, 0}, { 4, 6, 4, 0}, { 0,14, 4, 0}, { 4,12, 4, 0}}, // T
  {{12, 6, 0, 0}, { 2, 6, 4, 0}, { 0,12, 6, 0}, { 4,12, 8, 0}}
};

int tetroIndex; // Which tetro is currently active
int tetroRotation; // Rotation of the active tetro
int tetroRow; // Row offset of the active tetro
int tetroColumn; // Column offset of the active tetro
uint16_t renderedTetro[4];

void setup() {
  // Set up the RNG
  randomSeed(analogRead(A0));

  // Initialise the screen
  dmd.setBrightness(255);
  dmd.begin();

  // Set up the playfield.
  dmdBuffer.fillScreen(true);
  for (int i=0; i<2; i++) {
    dmd.swapBuffers(dmdBuffer);
    delay(100);
    dmd.swapBuffers(dmdBuffer);
    delay(100);
  }
  dmdBuffer.clearScreen();
  dmdBuffer.drawBox(0, 15, 21, 4);
  dmd.swapBuffers(dmdBuffer);
  dmdBuffer.drawBox(0, 15, 21, 4);
  // Set bits to the left and right of the playfield to 1 so
  // we can use them as a mask when doing bounds checking.
  for (int i=0; i<playFieldRows; i++) {
    playField[i] = 57351;
  }
  // Set the last row to all solid to act as a floor.
  // Nothing will move below it.
  playField[playFieldRows-1] = 65535;

  renderPlayField();

  // Test. Let's set up some tetrominoes and render them.
  // tetroRotation = 0; // This is the default rotation.
  // tetroColumn = 6; // This is the default position
  newTetro();

  if (expandTetro()) {
    renderTetro(GRAPHICS_ON);
    dmd.swapBuffers(dmdBuffer);
    dmdBuffer.copyFrame(dmd, 0, 0);
  }
}

void loop() {
  delay(50);
  renderTetro(GRAPHICS_OFF);
  if (moveDown()) {
    renderTetro(GRAPHICS_ON);
    dmd.swapBuffers(dmdBuffer);
    dmdBuffer.copyFrame(dmd, 0, 0);
  } else {
    // tetro is down.
    // add to playfield.
    renderTetro(GRAPHICS_ON);
    addTetroToPlayfield();
    // Set up new tetro.
    newTetro();
  }
}

// Generate a new (random) tetronimo.
void newTetro() {
  tetroIndex = random(7);
  tetroColumn = random(4, 11);
  tetroRotation = random(4);
  tetroRow = 0;
}

// Render the playField and completed rows.
void renderPlayField() {
  uint16_t row;
  for (int idx=4; idx<(playFieldRows-1); idx++) {
    row = playField[idx];
    for (int y=3; y<13; y++) {
      if ((row>>y)&1) {
        // Offset for idx is number of "hidden" rows - 1.
        dmdBuffer.setPixel(idx-3, y+2, GRAPHICS_ON);
      } else {
        dmdBuffer.setPixel(idx-3, y+2, GRAPHICS_OFF);
      }
    }
  }
  dmd.swapBuffers(dmdBuffer);
  dmdBuffer.copyFrame(dmd, 0, 0);
}

void renderTetro(DMDGraphicsMode mode) {
  uint16_t row;
  int rowidx;
  for (int idx=0; idx<4; idx++) {
    rowidx = tetroRow+idx-3;
    if (rowidx>0) { // Avoid drawing anything in real row 0;
      row = renderedTetro[idx];
      for (int y=3; y<13; y++) {
        if ((row>>y)&1) {
          // Offset for idx is number of "hidden" rows - 1.
          dmdBuffer.setPixel(rowidx, y+2, mode);
        }
      }
    }
  }
}

void addTetroToPlayfield() {
  uint16_t row;
  for (int idx=0; idx<4; idx++) {
    row = renderedTetro[idx];
    playField[tetroRow+idx] = playField[tetroRow+1] | row;
  }
}

void moveLeft() {
  // Nothing yet
}

void moveRight() {
  // Nothing yet
}

boolean moveDown() {
  tetroRow++;
  if (expandTetro()) {
    return true;
  } else {
    tetroRow--;
    return false;
  }
}

boolean rotateClockwise() {
  int oldRotation = tetroRotation;
  tetroRotation = (tetroRotation + 1) % 4;
  if (expandTetro()) {
    return true;
  } else {
    tetroRotation = oldRotation;
    return false;
  }
}

boolean rotateAnticlockwise() {
  int oldRotation = tetroRotation;
  tetroRotation = (tetroRotation + 3) %4;
  if (expandTetro()) {
    return true;
  } else {
    tetroRotation = oldRotation;
    return false;
  }
}

// Render the new tetro and make sure it's valid.
// TODO: wall kicks
boolean expandTetro() {
  uint16_t newRenderedTetro[4];
  for (int i=0; i<4; i++) {
    newRenderedTetro[i] = ((uint16_t)tetros[tetroIndex][tetroRotation][i])
      << tetroColumn;
    // Verify the new row is in a valid position:
    if ((newRenderedTetro[i] & playField[tetroRow+i]) > 0) {
      return false;
    }
  }
  // All rows valid, make the new tetro the current one.
  memcpy(renderedTetro, newRenderedTetro, sizeof(uint16_t)*4);
  return true;
}